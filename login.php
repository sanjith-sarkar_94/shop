<?php include "inc/header.php"; 


$login = Session::get('custlogin');
if ($login == true) {
	header("location: order.php");
}


// Customer Login

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['login'])) {
	$username = $_POST['username'];
	$password = $_POST['password'];

	$userLogin = $cmr->userLogin($username, $password);
}

// Customer Register

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['register'])) {
	$username = $_POST['username'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$country = $_POST['country'];
	$zip = $_POST['zip'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$pass = $_POST['pass'];


	$cstreg = $cmr->customerRegistration($_POST);
}


?>

<div class="main">
	<div class="content">
		<div class="login_panel">
			<h3>Existing Customers</h3>
			<p>Sign in with the form below.</p>
			<form action="" method="post">
				<input name="username" type="text" placeholder="Enter username">
				<input name="password" type="password" placeholder="Enter Password" >
				<div class="buttons"><div><button type="submit" class="grey" name="login">Sign In</button></div></div>
				<p class="note">If you forgot your passoword just enter your email and click <a href="#">here</a></p>
			</form>
		</div>
		<div class="register_account">
			<h3>Register New Account</h3>
			<?php if (isset($cstreg)) {
				echo $cstreg;
			} ?>
			<form action="" method="post">
				<table>
					<tbody>
						<tr>
							<td>
								<div>
									<input type="text" name="username" placeholder="Enter Customer Name">
								</div>

								<div>
									<input type="text" name="address" placeholder="Enter Customer Address">
								</div>

								<div>
									<input type="text" name="city" placeholder="Enter City Name">
								</div>
								<div>
									<select id="country" name="country">
										<option value="">Select a Country</option>         
										<option value="AF">Afghanistan</option>
										<option value="AL">Albania</option>
										<option value="DZ">Algeria</option>
										<option value="AR">Argentina</option>
										<option value="AM">Armenia</option>
										<option value="AW">Aruba</option>
										<option value="AU">Australia</option>
										<option value="AT">Austria</option>
										<option value="AZ">Azerbaijan</option>
										<option value="BS">Bahamas</option>
										<option value="BH">Bahrain</option>
										<option value="BD">Bangladesh</option>

									</select>
								</div>
							</td>
							<td>
								<div>
									<input type="text" name="zip" placeholder="Enter zip Number">
								</div>
								<div>
									<input type="text" name="phone" placeholder="Enter Phone Number">
								</div>		        

								<div>
									<input type="text" name="email" placeholder="Enter Customer Email">
								</div>
								<div>
									<input type="password" name="pass" placeholder="Enter Password">
								</div>
								
							</td>
						</tr> 
					</tbody></table> 
					<div class="search"><div><button class="grey" name="register">Create Account</button></div></div>
					<p class="terms">By clicking 'Create Account' you agree to the <a href="#">Terms &amp; Conditions</a>.</p>
					<div class="clear"></div>
				</form>
			</div>  	
			<div class="clear"></div>
		</div>
	</div>
</div>

<?php include "inc/footer.php"; ?>