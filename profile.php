<?php include "inc/header.php"; 

// get Customer id from session

$cmrId = Session::get('cmrId');

// get customer data

$getcustomer = $cmr->getCustomerData($cmrId);

?>
<style>
	.tblone{width: 550px; margin: 0 auto; border: 2px solid #ddd;}
	.tblone tr td{text-align: center;}
	.tblone tr td a{text-align: center;}
</style>
<div class="main">
	<div class="content">
		<div class="section group">			
			<table class="tblone">
				<?php if ($getcustomer) { 
					while($result = $getcustomer->fetch_assoc()){
					?>
				<tr><td colspan="3"><h2>Profile Details</h2></td></tr>
				<tr>
					<td width="20%">Name</td>
					<td width="10%">:</td>
					<td width="30%"><?php echo $result['username'] ?></td>
				</tr>
				<tr>
					<td width="20%">Address</td>
					<td width="5%">:</td>
					<td width="30%"><?php echo $result['address'] ?></td>
				</tr>
				<tr>
					<td width="20%">City</td>
					<td width="5%">:</td>
					<td width="30%"><?php echo $result['city'] ?></td>
				</tr>
				<tr>
					<td width="20%">Country</td>
					<td width="5%">:</td>
					<td width="30%"><?php echo $result['country'] ?></td>
				</tr>
				<tr>
					<td width="20%">Zip</td>
					<td width="5%">:</td>
					<td width="30%"><?php echo $result['zip'] ?></td>
				</tr>
				<tr>
					<td width="20%">Phone</td>
					<td width="5%">:</td>
					<td width="30%"><?php echo $result['phone'] ?></td>
				</tr>
				<tr>
					<td width="20%">Phone</td>
					<td width="5%">:</td>
					<td width="30%"><?php echo $result['email'] ?></td>
				</tr>
				<tr>
					<td></td>
					<td width="50%"><a href="editprofile.php">Update Profile</a></td>
					<td></td>
				</tr>
					<?php }
				} ?>
			</table>
		</div>
	</div>
</div>

<?php include "inc/footer.php"; ?>