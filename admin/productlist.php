﻿<?php include 'inc/header.php';
include 'inc/sidebar.php';
include 'classes/Product.php';

// Display all product

$product = new Product();

$getAllProduct = $product->getAllProduct();

// Delete Product

 if (isset($_GET['delid'])) {
 		$delid = $_GET['delid'];

 		$delproductbyid = $product->delproductbyid($delid);
 }
?>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Post List</h2>
		<?php if (isset($delproductbyid)) {
			echo $delproductbyid;
		} ?>
		<div class="block">  
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th>Serial No</th>
						<th>Product Name</th>
						<th>Category</th>
						<th>Brand</th>
						<th>Description</th>
						<th>price</th>
						<th>image</th>
						<th>type</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($getAllProduct)) {
						while($result = $getAllProduct->fetch_assoc()){ ?>
							<tr class="odd gradeX">
								<td><?php echo $result['productId']; ?></td>
								<td><?php echo $result['productName']; ?></td>
								<td><?php echo $result['catName']; ?></td>
								<td><?php echo $result['brandName']; ?></td>
								<td><?php echo $fm->textShorten($result['body'], 60); ?></td>
								<td><?php echo $result['price']; ?></td>
								<td><img src="<?php echo $result['image']; ?>" width="60px" height="60px"></td>
								<td> <?php 
                                if ($result['type'] == 1) {
                                     echo "Featured";
                                }else{
                                    echo "General";
                                }
                                 ?></td>
								<td><a href="editproduct.php?productId=<?php echo $result['productId']; ?>">Edit</a> || <a onclick="return confirm('are you sure to delete!')" href="?delid=<?php echo $result['productId']; ?>">Delete</a></td>
							</tr>
					<?php	}
					} ?>
				</tbody>
			</table>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		setupLeftMenu();
		$('.datatable').dataTable();
		setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>
