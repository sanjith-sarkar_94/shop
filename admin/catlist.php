﻿<?php include 'inc/header.php';
 include 'inc/sidebar.php';
 include "classes/Category.php";

 $cat = new Category();

 if (isset($_GET['delid'])) {
 		$delid = $_GET['delid'];

 		$delcatbyid = $cat->delcatbyid($delid);
 }
 ?>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Category List</h2>
		<div class="block">
		<?php if (isset($delcatbyid)) {
			 echo $delcatbyid;
		} ?>        
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th>Serial No.</th>
						<th>Category Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$getcat = $cat->getAllCat();
					if ($getcat) {
						while ($result = $getcat->fetch_assoc()) { ?>
							<tr class="odd gradeX">
								<td><?php echo $result['catId']; ?></td>
								<td><?php echo $result['catName']; ?></td>
								<td><a href="editcat.php?catid=<?php echo $result['catId']; ?>">Edit</a> || <a onclick="return confirm('are you sure to delete!')" href="?delid=<?php echo $result['catId']; ?>">Delete</a></td>
							</tr>
						<?php	}
					}
					?>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		setupLeftMenu();

		$('.datatable').dataTable();
		setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>

