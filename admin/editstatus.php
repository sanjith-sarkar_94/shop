<?php include 'inc/header.php';
include 'inc/sidebar.php';
include_once 'classes/Cart.php';

$cart = new Cart();

//get Order id 

if (!isset($_GET['orderId']) || $_GET['orderId'] == NULL) {
    echo "<script>window.location = 'orderlist.php';</script>";
}else{
    $orderId = $_GET['orderId'];
}

 // Update Status from order by order id

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {

 $updateStatus = $cart->updateStatus($_POST, $orderId);
}

// get order data from order by id

$getOrderByid = $cart->getOrderByid($orderId);
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Status</h2>
        <?php 
        if (isset($updateStatus)) {
            echo $updateStatus;
        }
        ?>
        <div class="block">               
         <form action="" method="post" enctype="multipart/form-data">
            <table class="form">
                <?php 
                if ($getOrderByid) {
                    while ($getOrder = $getOrderByid->fetch_assoc()) { ?>
                        <tr>
                            <td>
                                <label>Customer Id</label>
                            </td>
                            <td>
                                <input type="text" name="customerId" value="<?php echo $getOrder['cmrId'] ?>" class="medium" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Status</label>
                            </td>
                            <td>
                                <select id="select" name="status">

                                   <option><?php 
                                   if ($getOrder['status'] == "0") {
                                    echo "Pending";
                                }elseif ($getOrder['status'] == "1") {
                                    echo "Shipped";
                                }else{
                                    echo "Deliverd";
                                }
                                ?></option> 
                                <option value="1">Pending</option>
                                <option value="1">Shipped</option>
                                <option value="2">Delivered</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="update" Value="Update" />
                        </td>
                    </tr>
                <?php   }
            }
            ?>
        </table>
    </form>
</div>
</div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>


