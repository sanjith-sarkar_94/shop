<?php 

/**
 * 
 */

class Category
{
	private $db;
	private $fm;
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}

// Add Category

	public function addCategory($catname){
		$catname = $this->fm->validation($catname);

		$catname = mysqli_real_escape_string($this->db->link, $catname);

		if (empty($catname)) {
			echo "<div style='text-align:center'><span class='btn btn-success'><strong>Error!!</strong> Sorry, Field must not empty!!</span></div>";
		}else{
			$sql = "INSERT INTO tbl_category (catName) VALUES('$catname')";
			$insertCat = $this->db->insert($sql);
			if ($insertCat) {
				echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Category Inserted Successfully</span></div>";
			}else{
				echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Category Not Inserted</span></div>";
			}
		}
	}


// Show Category


	public function getAllCat(){
		$sql = "SELECT * FROM tbl_category ORDER by catId ASC";
		$catlist = $this->db->select($sql);
		return $catlist;
	}


	// Edit Category


	public function getCatByid($catid){
		$sql = "SELECT * FROM tbl_category WHERE catId = '$catid'";
		$catinfo = $this->db->select($sql);
		return $catinfo;
	}


	// Update Category


	public function updateCatByid($catid, $catname){
		$catname = $this->fm->validation($catname);

		$catname = mysqli_real_escape_string($this->db->link, $catname);
		
		$sql = "UPDATE tbl_category SET 
		catName = '$catname'
		 WHERE catId = '$catid'";
		$updcat = $this->db->update($sql);
		if ($updcat) {
			echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Category Updated Successfully</span></div>";
		}else{
			echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Category Not Updated</span></div>";
		}		
	}

	// Delete Category

	public function delcatbyid($catid){
		$sql = "DELETE FROM tbl_category WHERE catId = '$catid'";
		$delcat = $this->db->delete($sql);
		if ($delcat) {
			echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Category Deleted Successfully</span></div>";
		}else{
			echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Category Not Deleted!!</span></div>";
		}		
	}
}
?>