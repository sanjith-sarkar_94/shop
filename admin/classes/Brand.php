<?php 
/**
 * 
 */

class Brand
{
	private $db;
	private $fm;
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}


	// Add Brand

	public function addbrand($brandname){
		$brandname = $this->fm->validation($brandname);

		$brandname = mysqli_real_escape_string($this->db->link, $brandname);

		if (empty($brandname)) {
			echo "<div style='text-align:center'><span class='btn btn-success'><strong>Error!!</strong> Sorry, Field must not empty!!</span></div>";
		}else{
			$sql = "INSERT INTO tbl_brand (brandName) VALUES('$brandname')";
			$insertbrand = $this->db->insert($sql);
			if ($insertbrand) {
				echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Brand Inserted Successfully</span></div>";
			}else{
				echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Brand Not Inserted</span></div>";
			}
		}
	}


	// Brand list


	public function getAllbrand(){
		$sql = "SELECT * FROM tbl_brand ORDER by brandId ASC";
		$brandlist = $this->db->select($sql);
		return $brandlist;
	}


	// find Brand by id 


	public function getBrandByid($brandid){
		$sql = "SELECT * FROM tbl_brand WHERE brandId = '$brandid'";
		$brandlist = $this->db->select($sql);
		return $brandlist;
	}


	// Update Brand


	public function updateBrandByid($brandid, $brandname){
		$sql = "UPDATE tbl_brand SET 
		brandName = '$brandname'
		WHERE brandId = '$brandid'
		";
		$updbrand = $this->db->update($sql);
		if ($updbrand) {
				echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Brand Updated Successfully</span></div>";
			}else{
				echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Brand Not Updated</span></div>";
			}	
	}

	// Delete Brand

	public function delbrandbyid($delid){
		$sql = "DELETE FROM tbl_brand WHERE brandId = '$delid'";
		$delbrand = $this->db->delete($sql);
		if ($delbrand) {
			echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Brand Deleted Successfully</span></div>";
		}else{
			echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Brand Not Deleted!!</span></div>";
		}		
	}

}
 ?>