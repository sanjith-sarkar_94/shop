<?php 
/**
  * 
  */
class Customer
{

	private $db;
	private $fm;
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}


	// Email Checking

	// public function emailChecking($email){
	// 	$sql = "SELECT * FROM tbl_customer WHERE email = '$email";
	// 	$query = $this->db->select($sql);
	// 	if ($query) {
	// 		return $query;
	// 	}else{
	// 		return false;
	// 	}
	// }


	// User Registration

	public function customerRegistration($data){
		$username = $this->fm->validation($data['username']);
		$address = $this->fm->validation($data['address']);
		$city = $this->fm->validation($data['city']);
		$country = $this->fm->validation($data['country']);
		$zip = $this->fm->validation($data['zip']);
		$phone = $this->fm->validation($data['phone']);
		$email = $this->fm->validation($data['email']);
		$pass = $this->fm->validation($data['pass']);

		$username = mysqli_real_escape_string($this->db->link, $username);
		$address = mysqli_real_escape_string($this->db->link, $address);
		$city = mysqli_real_escape_string($this->db->link, $city);
		$country = mysqli_real_escape_string($this->db->link, $country);
		$zip = mysqli_real_escape_string($this->db->link, $zip);
		$phone = mysqli_real_escape_string($this->db->link, $phone);
		$email = mysqli_real_escape_string($this->db->link, $email);
		$pass = mysqli_real_escape_string($this->db->link, $pass);

		//$chk_email = $this->emailChecking($email);

		if ($username == "" OR $address == "" OR $city == "" OR $country == "" OR $zip == "" OR $phone == "" OR $email == "" OR $pass == "") {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not empty!!</div>";
			return $msg;
		}

		// if ($chk_email == true) {
		// 	$msg = "<div class= 'alert alert-danger'><strong>Error!<strong>Email already exits!!</div>";
		// 	return $msg;
		// }

	   $sql = "INSERT INTO tbl_customer(username, address, city, country, zip, phone, email, password) values('$username', '$address', 'city', 'country', 'zip', '$phone', '$email', '$pass')";
		$insert_user = $this->db->insert($sql);
		if ($insert_user) {
			echo "<div class='alert alert-success'><strong>Success!<strong> Thank you, You have been registerted</div>";
		}else{
			echo "<div class='alert alert-Error'><strong>Error!<strong>Sorry, There have been something error to register details</div>";
		}

	}


	// User Login

	public function userLogin($username, $password){
		$username = $this->fm->validation($username);
		$password = $this->fm->validation($password);

		$username = mysqli_real_escape_string($this->db->link, $username);
		$password = mysqli_real_escape_string($this->db->link, $password);

		$sql = "SELECT * FROM tbl_customer WHERE username = '$username' AND password = '$password'";
				$result = $this->db->select($sql);
				if ($result == true) {
					$value  = $result->fetch_assoc();

					Session::set("custlogin", true);
					Session::set("username", $value['username']);
					Session::set("cmrId", $value['id']);
					Session::set("loginmsg",  "<div style='text-align:center;'><span class='btn bg-success;'><strong>Success!!</strong> Thank You, You are logged in!!.</span></div>");
					header("Location: index.php");
					
					
				}else{
					echo "<span>Sorry! username or password does not matched!</span>";
				}
	}


	// Get Customer data from customer by session id

	public function getCustomerData($cmrId){
			$sql = "SELECT * FROM tbl_customer WHERE id = '$cmrId'";
				$getuser = $this->db->select($sql);
				return $getuser;
	}


	// Update Customer profile


	public function updateUserProfile($data, $cmrId){
		$username = $this->fm->validation($data['username']);
		$address = $this->fm->validation($data['address']);
		$city = $this->fm->validation($data['city']);
		$country = $this->fm->validation($data['country']);
		$zip = $this->fm->validation($data['zip']);
		$phone = $this->fm->validation($data['phone']);
		$email = $this->fm->validation($data['email']);

		$username = mysqli_real_escape_string($this->db->link, $username);
		$address = mysqli_real_escape_string($this->db->link, $address);
		$city = mysqli_real_escape_string($this->db->link, $city);
		$country = mysqli_real_escape_string($this->db->link, $country);
		$zip = mysqli_real_escape_string($this->db->link, $zip);
		$phone = mysqli_real_escape_string($this->db->link, $phone);
		$email = mysqli_real_escape_string($this->db->link, $email);


		//$chk_email = $this->emailChecking($email);

		if ($username == "" OR $address == "" OR $city == "" OR $country == "" OR $zip == "" OR $phone == "" OR $email == "") {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not empty!!</div>";
			return $msg;
		}

		// if ($chk_email == true) {
		// 	$msg = "<div class= 'alert alert-danger'><strong>Error!<strong>Email already exits!!</div>";
		// 	return $msg;
		// }

	   $sql = "UPDATE tbl_customer set
	   username = '$username',
	   address = '$address',
	   city = '$city',
	   country = '$country',
	   zip = '$zip',
	   phone = '$phone',
	   email = '$email'
	   WHERE id = '$cmrId'";
		$insert_user = $this->db->insert($sql);
		if ($insert_user) {
			//echo "<div class='alert alert-success'><strong>Success!<strong> Thank you, Profile Updated Sucessfully</div>";
			header("location: profile.php");
		}else{
			echo "<div class='alert alert-Error'><strong>Error!<strong>Sorry, There have been something error to register details</div>";
		}

	}

} ?>