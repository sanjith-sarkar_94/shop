<?php 
/**
 * 
 */

class Product
{
	
	private $db;
	private $fm;
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function insertProduct($data, $file){

		$productName = $this->fm->validation($data['productName']);
		$category = $this->fm->validation($data['category']);
		$brand = $this->fm->validation($data['brand']);
		$body = $this->fm->validation($data['body']);
		$price = $this->fm->validation($data['price']);
		$type = $this->fm->validation($data['type']);

		$productName = mysqli_real_escape_string($this->db->link, $productName);
		$category = mysqli_real_escape_string($this->db->link, $category);
		$brand = mysqli_real_escape_string($this->db->link, $brand);
		$body = mysqli_real_escape_string($this->db->link, $body);
		$price = mysqli_real_escape_string($this->db->link, $price);
		$type = mysqli_real_escape_string($this->db->link, $type);

		
		$permited  = array('jpg', 'jpeg', 'png', 'gif');
		$file_name = $file['image']['name'];
		$file_size = $file['image']['size'];
		$file_temp = $file['image']['tmp_name'];

		$div = explode('.', $file_name);
		$file_ext = strtolower(end($div));
		$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
		$uploaded_image = 'upload/'.$unique_image;

		if ($productName == "" || $category == "" || $brand == "" || $body == "" || $price == "" || $type == "") {
			echo "<span class='error'>Field must no empty!</span>";
		}elseif ($file_size >1048567) {
			echo "<span class='error'>Image Size should be less then 1MB!</span>";
		} elseif (in_array($file_ext, $permited) === false) {
			echo "<span class='error'>You can upload only:-"
			.implode(', ', $permited)."</span>";
		} else{
			move_uploaded_file($file_temp, $uploaded_image);
			$query = "INSERT INTO tbl_product(productName, catId, brandId, body, price, image, type) VALUES('$productName', '$category', '$brand', '$body', '$price', '$uploaded_image', '$type')";
			$inserted_rows = $this->db->insert($query);

			if ($inserted_rows) {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Product Inserted Successfully.
				</span></div>";
			}else {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Product Not Inserted Successfully.
				</span></div>";
			}
		}

	}


	// Display all product 


	public function getAllProduct(){

		//$sql = "SELECT * FROM tbl_product";
		// $sql = "SELECT tbl_product.*, tbl_category.catName, tbl_brand.brandName from tbl_product
		// INNER JOIN tbl_category ON tbl_product.catId = tbl_category.catId
		// INNER JOIN tbl_brand ON tbl_product.brandId = tbl_brand.brandId
		// ";

		$sql = "SELECT p.*, c.catName, b.brandName FROM tbl_product AS p, tbl_category AS c, tbl_brand AS b 
		WHERE p.catId = c.catId AND p.brandId = b.brandId";
		$getProduct = $this->db->select($sql);
		return $getProduct;
	}


	// Display product by id


	public function getproductByid($productId){
		$sql = "SELECT * FROM tbl_product WHERE productId = '$productId'";
		$getprodutbyid = $this->db->select($sql);
		return $getprodutbyid;
	}


	// Update Product


	public function updateProduct($data, $file, $productId){

		$productName = $this->fm->validation($data['productName']);
		$category = $this->fm->validation($data['category']);
		$brand = $this->fm->validation($data['brand']);
		$body = $this->fm->validation($data['body']);
		$price = $this->fm->validation($data['price']);
		$type = $this->fm->validation($data['type']);

		$productName = mysqli_real_escape_string($this->db->link, $productName);
		$category = mysqli_real_escape_string($this->db->link, $category);
		$brand = mysqli_real_escape_string($this->db->link, $brand);
		$body = mysqli_real_escape_string($this->db->link, $body);
		$price = mysqli_real_escape_string($this->db->link, $price);
		$type = mysqli_real_escape_string($this->db->link, $type);

		
		$permited  = array('jpg', 'jpeg', 'png', 'gif');
		$file_name = $file['image']['name'];
		$file_size = $file['image']['size'];
		$file_temp = $file['image']['tmp_name'];

		$div = explode('.', $file_name);
		$file_ext = strtolower(end($div));
		$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
		$uploaded_image = 'upload/'.$unique_image;

		if ($productName == "" || $category == "" || $brand == "" || $body == "" || $price == "" || $type == "") {
			echo "<span class='error'>Field must no empty!</span>";
		}else{
			if (!empty($file_name)) {
				if ($file_size >1048567) {
					echo "<span class='error'>Image Size should be less then 1MB!</span>";
				} elseif (in_array($file_ext, $permited) === false) {
					echo "<span class='error'>You can upload only:-"
					.implode(', ', $permited)."</span>";
				} else{
					move_uploaded_file($file_temp, $uploaded_image);
					
					$query = "UPDATE tbl_product SET 
					productName = '$productName',
					catId = '$category',
					brandId = '$brand',
					body = '$body',
					price = '$price',
					image = '$uploaded_image',
					type = '$type'
					WHERE productId = '$productId'
					";

					$updateproduct = $this->db->update($query);

					if ($updateproduct) {
						echo "<div style='text-align:center;'><span class='btn bg-success;'>Product Updated Successfully.
						</span></div>";
					}else {
						echo "<div style='text-align:center;'><span class='btn bg-success;'>Product Not Updated Successfully.
						</span></div>";
					}
				}
			}else{
				$query = "UPDATE tbl_product SET 
				productName = '$productName',
				catId = '$category',
				brandId = '$brand',
				body = '$body',
				price = '$price',
				type = '$type'
				WHERE productId = '$productId'
				";

				$updateproduct = $this->db->update($query);

				if ($updateproduct) {
					echo "<div style='text-align:center;'><span class='btn bg-success;'>Product Updated Successfully.
					</span></div>";
				}else {
					echo "<div style='text-align:center;'><span class='btn bg-success;'>Product Not Updated Successfully.
					</span></div>";
				}
			}

		}
	}




	//    Delete Product

	public function delproductbyid($delid){

		$query = "SELECT * FROM tbl_product WHERE productId = '$delid'";
		$getproduct = $this->db->select($query);
		if ($getproduct) {
			while ($delimage = $getproduct->fetch_assoc()) {
				$dellink = $delimage['image'];
				unlink($dellink);
			}
		}

		$sql = "DELETE FROM tbl_product WHERE productId = '$delid'";
		$delproduct = $this->db->delete($sql);
		if ($delproduct) {
			echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Product Deleted Successfully</span></div>";
		}else{
			echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Product Not Deleted!!</span></div>";
		}		
	}



	// Get Featured Product

	public function getfeaturedproduct(){
		$sql = "SELECT * FROM tbl_product WHERE type = '1' ORDER BY productId DESC LIMIT 4";
		$getprodutbyid = $this->db->select($sql);
		return $getprodutbyid;
	}


	//Get Net Product

	public function getnewproduct(){
		$sql = "SELECT * FROM tbl_product ORDER BY productId DESC LIMIT 4";
		$getProduct = $this->db->select($sql);
		return $getProduct;
	}


	// get single product by id


	public function getsingleproduct($productId){

		$sql = "SELECT p.*, c.catName, b.brandName FROM tbl_product AS p, tbl_category AS c, tbl_brand AS b 
		WHERE p.catId = c.catId AND p.brandId = b.brandId AND p.productId = '$productId' LIMIT 1";
		$getProduct = $this->db->select($sql);
		return $getProduct;
	}



	// get latest Apple from Product


	public function latestFromApple(){
		$sql = "SELECT * FROM tbl_product WHERE brandId = '6' ORDER BY productId DESC LIMIT 1";
		$getProduct = $this->db->select($sql);
		return $getProduct;
	}


	// get latest Dell from Product


	public function latestFromHP(){
		$sql = "SELECT * FROM tbl_product WHERE brandId = '1' ORDER BY productId DESC LIMIT 1";
		$getProduct = $this->db->select($sql);
		return $getProduct;
	}


	// get latest Asus from Product


	public function latestFromAsus(){
		$sql = "SELECT * FROM tbl_product WHERE brandId = '5' ORDER BY productId DESC LIMIT 1";
		$getProduct = $this->db->select($sql);
		return $getProduct;
	}


	// get latest Asus from Product


	public function latestFromSamsung(){
		$sql = "SELECT * FROM tbl_product WHERE brandId = '8' ORDER BY productId DESC LIMIT 1";
		$getProduct = $this->db->select($sql);
		return $getProduct;
	}


	// add Wishlist 

	public function addToWlist($productId, $cmrId){
		$login = Session::get('custlogin');
		if ($login == false) {
			$msg = "<div style='text-align:center;'><span class='btn bg-Sorry;'><strong>Sorry!</strong> Please login your account!.
						</span></div>";
			return $msg;
		}else{
			$sql = "SELECT * FROM tbl_product WHERE productId = '$productId' ORDER BY productId DESC";
			$getProduct = $this->db->select($sql);
			if ($getProduct) {
				while ($result = $getProduct->fetch_assoc()) {
					$productId = $result['productId'];
					$productName = $result['productName'];
					$price = $result['price'];
					$image = $result['image'];

					$query = "INSERT INTO tbl_wlist(customerId, productId, productName, price, image) VALUES('$cmrId', '$productId', '$productName', '$price', '$image')";
					$inserted_rows = $this->db->insert($query);

					if ($inserted_rows) {
						echo "<div style='text-align:center;'><span class='btn bg-success;'>Wishlist Inserted Successfully.
						</span></div>";
					}else {
						echo "<div style='text-align:center;'><span class='btn bg-success;'>Wishlist Not Inserted Successfully.
						</span></div>";
					}
				}
			}
		}
	}


	// get Wistlist from wlist by customer id


	public function checkWlist($cmrId){
		$sql = "SELECT * FROM tbl_wlist WHERE customerId = '$cmrId' ORDER BY id DESC";
		$getWlist = $this->db->select($sql);
		return $getWlist;
	}
}

?>