<?php 
/**
 * 
 */
class Cart
{
	private $db;
	private $fm;

	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}


	// product add to cart

	public function addtocart($quantity, $productId){

		$quantity = $this->fm->validation($_POST['quantity']);

		$quantity = mysqli_real_escape_string($this->db->link, $quantity);
		$productId = mysqli_real_escape_string($this->db->link, $productId);
		$sId = session_id();

		$sql = "SELECT * FROM tbl_product WHERE productId = '$productId'";
		$getproduct = $this->db->select($sql)->fetch_assoc();

		$productName = $getproduct['productName'];
		$price = $getproduct['price'];
		$image = $getproduct['image'];

		$query = "SELECT * FROM tbl_cart WHERE productId = '$productId' AND sId = '$sId'";
		$chkproduct = $this->db->select($query);
		if ($chkproduct) {
			$msg = "Product Already Added!";
			return $msg;
		}else{
			$query = "INSERT INTO tbl_cart(sId, productId, productName, price, quantity, image) values('$sId', '$productId', '$productName', '$price', '$quantity', '$image')";

			$insert_cart = $this->db->insert($query);
			if ($insert_cart) {
				header("Location: cart.php");
			}else{
				header("Location: 404.php");
			}
		}

	}



	// get product by session


	public function getcartproduct(){
		$sId = session_id();
		$sql = "SELECT * FROM tbl_cart WHERE sId = '$sId' ORDER BY cartId DESC";
		$getcart = $this->db->select($sql);
		return $getcart;
		
	}


	// Update quantity

	public function updatequantity($cartId, $quantity){
		$cartId = $this->fm->validation($_POST['cartId']);
		$quantity = $this->fm->validation($_POST['quantity']);

		$cartId = mysqli_real_escape_string($this->db->link, $cartId);
		$quantity = mysqli_real_escape_string($this->db->link, $quantity);

		$sql = "UPDATE tbl_cart SET 
		quantity = '$quantity'
		WHERE cartId = '$cartId';
		";
		$update_quantity = $this->db->update($sql);
		if ($update_quantity) {
			header("Location: cart.php");
		}else{
			echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Quantity Not Updated</span></div>";
		}		
	}


	// delete cart by cartid

	public function delcartbyid($cartId){
		$sql = "DELETE FROM tbl_cart WHERE cartId = '$cartId'";
		$delcart = $this->db->delete($sql);
		if ($delcart) {
			header("Location: cart.php");
		}else{
			echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Cart Not Deleted!!</span></div>";
		}
	}



	// Delete Cart product

	public function delcartproduct(){
		$sql = "DELETE FROM tbl_cart";
		$delcart = $this->db->delete($sql);
		return $delcart;
	}



	// checkcarttable

	public function checkcarttable(){
		$sId = session_id();
		$sql = "SELECT * FROM tbl_cart WHERE sId = '$sId' ORDER BY cartId DESC";
		$cart = $this->db->select($sql);
		return $cart;
	}

	//Order Now

	public function orderproduct($cmrId){
		$sId = session_id();
		$sql = "SELECT * FROM tbl_cart WHERE sId = '$sId' ORDER BY cartId DESC";
		$getcart = $this->db->select($sql);
		if ($getcart) {
			while ($result = $getcart->fetch_assoc()) {

				$productId = $result['productId'];
				$productName = $result['productName'];
				$quantity = $result['quantity'];
				$price = $result['price'] * $quantity;
				$image = $result['image'];
				$productId = $result['productId'];

				$query = "INSERT INTO tbl_order(cmrId, productId, productName, price, quantity, image) values('$cmrId', '$productId', '$productName', '$price', '$quantity', '$image')";

				$insert_order = $this->db->insert($query);
				if ($insert_order) {
					header("location: success.php");
				}else{
					header("location: 404.php");

				}
			}
		}

	}


	// Customer Payment

	public function payableorder($cmrId){
		$sql = "SELECT * FROM tbl_order WHERE cmrId = '$cmrId' AND date = now()";
		$getorder = $this->db->select($sql);
		return $getorder;
	}



	// get orderlist from cart by customer id

	public function getOrderFromCart($cmrId){
		$sql = "SELECT * FROM tbl_order WHERE cmrId = '$cmrId' ORDER BY id DESC";
		$getorder = $this->db->select($sql);
		return $getorder;
	}


	// get all order from order

	public function getAllOrder(){
		$sql = "SELECT o.*, c.username FROM tbl_order AS o, tbl_customer AS c 
		WHERE o.cmrId = c.id";
		$getOrder = $this->db->select($sql);
		return $getOrder;
	}



	// get order data from order by id

	public function getOrderByid($orderId){
		$sql = "SELECT * FROM tbl_order WHERE id = '$orderId' ORDER BY id DESC";
		$getorder = $this->db->select($sql);
		return $getorder;
	}


	// update status from order by order id

	public function updateStatus($data, $orderId){
		$customerId = $this->fm->validation($data['customerId']);
		$status = $this->fm->validation($_POST['status']);

		$customerId = mysqli_real_escape_string($this->db->link, $customerId);
		$status = mysqli_real_escape_string($this->db->link, $status);

		$sql = "UPDATE tbl_order SET 
		cmrId = '$customerId',
		status = '$status'
		WHERE id = '$orderId';
		";
		$update_status = $this->db->update($sql);
		if ($update_status) {
			echo "<div style='text-align:center'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Status Updated Successfully</span></div>";
		}else{
			echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Quantity Not Updated</span></div>";
		}		
	}



	
}
?>