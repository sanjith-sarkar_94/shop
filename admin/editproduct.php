<?php include 'inc/header.php';
include 'inc/sidebar.php';
include_once 'classes/Product.php';
include_once 'classes/Category.php';
include_once 'classes/Brand.php';

$product = new Product();

//get Product id 

if (!isset($_GET['productId']) || $_GET['productId'] == NULL) {
    echo "<script>window.location = 'productlist.php';</script>";
}else{
    $productId = $_GET['productId'];
}

 // Update Product

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {

 $updateProduct = $product->updateProduct($_POST, $_FILES, $productId);
}

$getproductbyid = $product->getproductByid($productId);
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Product</h2>
        <?php 
        if (isset($insertProduct)) {
            echo $insertProduct;
        }
        ?>
        <div class="block">               
         <form action="" method="post" enctype="multipart/form-data">
            <table class="form">
                <?php 
                if ($getproductbyid) {
                    while ($getproduct = $getproductbyid->fetch_assoc()) { ?>
                    <tr>
                        <td>
                            <label>Name</label>
                        </td>
                        <td>
                            <input type="text" name="productName" value="<?php echo $getproduct['productName'] ?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <select id="select" name="category">
                                <?php 
                                $cat = new Category();
                                $catlist = $cat->getAllCat();
                                if ($catlist) {
                                    while ($result = $catlist->fetch_assoc()) { ?>
                                        <option 
                                       <?php if ($getproduct['catId'] == $result['catId']){ ?>
                                        selected = "selected"
                                      <?php } 
                                        ?>
                                         value= "<?php echo $result['catId']; ?>"> <?php echo $result['catName']; ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Brand</label>
                        </td>
                        <td>
                            <select id="select" name="brand">
                                <?php 
                                $brand = new Brand();
                                $brandlist = $brand->getAllbrand();
                                if ($brandlist) {
                                    while ($result = $brandlist->fetch_assoc()) { ?>
                                        <option
                                        <?php if ($getproduct['brandId'] == $result['brandId']) { ?>
                                            selected = "selected"
                                        <?php } ?>
                                         value= "<?php echo $result['brandId']; ?>"> <?php echo $result['brandName']; ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Description</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body"><?php echo $getproduct['body'] ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Price</label>
                        </td>
                        <td>
                            <input type="text" name="price" value="<?php echo $getproduct['price'] ?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <img src="<?php echo $getproduct['image'] ?>" width="80px" height="80px"></br>
                            <input type="file" name="image" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Product Type</label>
                        </td>
                        <td>
                            <select id="select" name="type">
                                
                               <option><?php 
                                if ($getproduct['type'] == 1) {
                                     echo "Featured";
                                }else{
                                    echo "General";
                                }
                                 ?></option> 
                                <option value="1">Featured</option>
                                <option value="2">General</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Update" />
                        </td>
                    </tr>
                <?php   }
            }
            ?>
        </table>
    </form>
</div>
</div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>


