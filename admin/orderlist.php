<?php include 'inc/header.php';
include 'inc/sidebar.php';
include 'classes/Cart.php';

// Display all product

$cart = new Cart();

$getOrder = $cart->getAllOrder();

// Delete Product

 if (isset($_GET['delid'])) {
 		$delid = $_GET['delid'];

 		$delproductbyid = $product->delproductbyid($delid);
 }
?>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Post List</h2>
		<?php if (isset($delproductbyid)) {
			echo $delproductbyid;
		} ?>

		<style>
			.data tr th{
				text-align: center;
			}
		</style>
		<div class="block" style="text-align: center;">  
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th width="5%">Serial</th>
						<th width="10%">Customer Id</th>
						<th width="10%">ProductId</th>
						<th width="15%">Product Name</th>
						<th width="5%">Quantity</th>
						<th width="10%">price</th>
						<th width="10%">image</th>
						<th width="15%">Date</th>
						<th width="10%">Status</th>
						<th width="10%">Change Status</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($getOrder)) {
						while($result = $getOrder->fetch_assoc()){ ?>
							<tr class="odd gradeX">
								<td><?php echo $result['id']; ?></td>
								<td><?php echo $result['cmrId']; ?></td>
								<td><?php echo $result['productId']; ?></td>
								<td><?php echo $result['productName']; ?></td>
								<td><?php echo $result['quantity']; ?></td>
								<td><?php echo $result['price']; ?></td>
								<td><?php echo $result['image']; ?></td>
								<td><?php echo $fm->dateFormat($result['date']); ?></td>
								<td>
									<?php if ($result['status'] == "0") {
											echo "Pending";
									}elseif ($result['status'] == "1") {
											echo "Shipped";
									}else{
										echo "Deliverd";
									} ?>
								</td>
								<td><a href="editstatus.php?orderId=<?php echo $result['id']; ?>">Edit</a> || <a onclick="return confirm('are you sure to delete!')" href="?delid=<?php echo $result['productId']; ?>">Delete</a></td>
							</tr>
					<?php	}
					} ?>
				</tbody>
			</table>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		setupLeftMenu();
		$('.datatable').dataTable();
		setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>
