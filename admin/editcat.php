<?php include 'inc/header.php';
include 'inc/sidebar.php';
include "classes/Category.php";

 // Add Category

if (!isset($_GET['catid']) || $_GET['catid'] == NULL) {
    echo "<script>window.location = 'catlist.php';</script>";
}else{
    $catid = $_GET['catid'];
}


$ca = new Category();

if ($_SERVER['REQUEST_METHOD'] == "POST"){
    $catname = $_POST['name'];

    $updatecat = $ca->updateCatByid($catid, $catname);

}
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Category</h2>
        <div class="block copyblock"> 
            <?php 
            if (isset($updatecat)) {
                echo $updatecat;
            }
             ?>
            <form action="" method="post">
                <table class="form">
                   <?php
                   $getcat = $ca->getCatByid($catid);
                   if ($getcat) {
                       while ($result = $getcat->fetch_assoc()) { ?>
                           <tr>
                <td>
                    <input type="text" name="name" class="medium" value="<?php echo $result['catName']; ?>" />
                </td>
            </tr>
            <tr> 
                <td>
                    <input type="submit" name="submit" Value="Save" /></td>
                </td>
            </tr>
                   <?php    }
                   }
               ?>     					
            </table>
            </form>
            </div>
            </div>
            </div>
            <?php include 'inc/footer.php';?>