<?php 
include "classes/Adminlogin.php";
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
	<?php 

	// Login page


	if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['login'])) {

		$username = $_POST['username'];
		$password = md5($_POST['password']);

		$al = new Adminlogin();

		$adminuser = $al->adminlogin($username, $password);
	}

	?>
	<div class="container">
		<section id="content">
			<form action="" method="post">
				<h1>Admin Login</h1>
				<div>
					<input type="text" placeholder="Username" required="" name="username"/>
				</div>
				<div>
					<input type="password" placeholder="Password" required="" name="password" />
				</div>
				<div>
					<input type="submit" name="login" value="Login" />
				</div>
			</form><!-- form -->
			<div class="button">
				<a href="forgotpass.php">Change Password!</a>
			</div>
			<div class="button">
				<a href="#">Dynamic Live Project!</a>
			</div>
		</section><!-- content -->
	</div><!-- container -->
</body>
</html>