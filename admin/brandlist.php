<?php include 'inc/header.php';
 include 'inc/sidebar.php';
 include "classes/Brand.php";

 $brand = new Brand();

 if (isset($_GET['delid'])) {
 		$delid = $_GET['delid'];

 		$delbrandbyid = $brand->delbrandbyid($delid);
 }
 ?>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Category List</h2>
		<div class="block">
		<?php if (isset($delcatbyid)) {
			 echo $delcatbyid;
		} ?>        
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th>Serial No.</th>
						<th>Category Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$getbrand = $brand->getAllbrand();
					if ($getbrand) {
						while ($result = $getbrand->fetch_assoc()) { ?>
							<tr class="odd gradeX">
								<td><?php echo $result['brandId']; ?></td>
								<td><?php echo $result['brandName']; ?></td>
								<td><a href="editbrand.php?brandid=<?php echo $result['brandId']; ?>">Edit</a> || <a onclick="return confirm('are you sure to delete!')" href="?delid=<?php echo $result['brandId']; ?>">Delete</a></td>
							</tr>
						<?php	}
					}
					?>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		setupLeftMenu();

		$('.datatable').dataTable();
		setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>

