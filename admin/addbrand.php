<?php include 'inc/header.php';
include 'inc/sidebar.php';
include "classes/Brand.php";

 // Add Category

$brand = new Brand();

if ($_SERVER['REQUEST_METHOD'] == "POST"){
    $brandname = $_POST['name'];

    $addbrand = $brand->addbrand($brandname);
}
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Brand</h2>
         <?php if (isset($addbrand)) {
                echo $addbrand;
            } ?>
        <div class="block copyblock"> 
            <form action="" method="post">
                <table class="form">					
                    <tr>
                        <td>
                            <input type="text" placeholder="Enter Brand Name..." name="name" class="medium" />
                        </td>
                    </tr>
                    <tr> 
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php include 'inc/footer.php';?>