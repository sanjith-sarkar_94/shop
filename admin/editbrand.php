<?php include 'inc/header.php';
include 'inc/sidebar.php';
include "classes/Brand.php";


$brand = new Brand();

 // get brand id

if (!isset($_GET['brandid']) || $_GET['brandid'] == NULL) {
    echo "<script>window.location = 'brnadlist.php';</script>";
}else{
    $brandid = $_GET['brandid'];
}

if ($_SERVER['REQUEST_METHOD'] == "POST"){
    $brandname = $_POST['name'];

    $updatebrand = $brand->updateBrandByid($brandid, $brandname);

} ?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Brand</h2>
        <div class="block copyblock"> 
            <?php 
            if (isset($updatebrand)) {
                echo $updatebrand;
            }
             ?>
            <form action="" method="post">
                <table class="form">
                   <?php
                   $getbrand = $brand->getBrandByid($brandid);
                   if ($getbrand) {
                       while ($result = $getbrand->fetch_assoc()) { ?>
                           <tr>
                <td>
                    <input type="text" name="name" class="medium" value="<?php echo $result['brandName']; ?>" />
                </td>
            </tr>
            <tr> 
                <td>
                    <input type="submit" name="submit" Value="Save" /></td>
                </td>
            </tr>
                   <?php    }
                   }
               ?>     					
            </table>
            </form>
            </div>
            </div>
            </div>
            <?php include 'inc/footer.php';?>