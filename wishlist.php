<?php include "inc/header.php"; 

// Custom login

$login = Session::get('custlogin');
if ($login == false) {
	header("location: login.php");
}

// get wishlist from wishlist by customer id

$getWlist = $pd->checkWlist($cmrId);
?>

<div class="main">
	<div class="content">
		<div class="section group">
			<table class="tblone">
				<tr>
					<th width="5%">Serial</th>
					<th width="10%">ProductId</th>
					<th width="10%">ProductName</th>
					<th width="10%">Price</th>
					<th width="15%">Image</th>					
					<th width="15%">Edit</th>
				</tr>

				<?php if ($getWlist) {
					while ($result = $getWlist->fetch_assoc()) { ?>
						<tr>
							<td><?php echo $result['id']; ?></td>
							<td><?php echo $result['productId']; ?></td>
							<td><?php echo $result['productName']; ?></td>
							<td>Tk. <?php echo $result['price']; ?></td>
							<td><img src="admin/<?php echo $result['image']; ?>" width="70px" height="60px" alt=""/></td>
							<td><a href="details.php?productId=<?php echo $result['productId']; ?>">Buy Now</a> ||
								<a onclick="return confirm('are you sure to delete!')" href="?delid=<?php echo $result['id']; ?>">Remove</a>
							</td>
						</tr>
					<?php	}
				} ?>
			</table>
		</div>
	</div>
</div>
<?php include "inc/footer.php"; ?>