<?php 
class Session{
	public static function init(){
		if(version_compare(phpversion(), '5.4.0', '<')){
			if (session_id() == '') {
				// code...
				session_start();
			}
		}else{
			if (session_status() == PHP_SESSION_NONE) {
				// code...
				session_start();
			}
		}
	}

	public static function set($key, $val){
		$_SESSION[$key] = $val;
	}

	public static function get($key){
		if (isset($_SESSION[$key])) {
			// code...
			return $_SESSION[$key];
		}else{
			return false;
		}
	}

	public static function destroy(){
		session_destroy();
		session_unset();
		header("LOCATION: login.php");
	}

	public static function checkSession(){
		if (self::get("login") == false) {
			self::destroy();
			header("location: login.php");
		}
	}

	public static function checkLogin(){
		if (self::get("login") == true) {
			header("location: index.php");
		}
	}
}
?>