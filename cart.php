<?php 
include "inc/header.php";

// Delete Cart

if (isset($_GET['delid'])) {
	$cartId = $_GET['delid'];

	$delcart = $ct->delcartbyid($cartId);
}

// update Quantity 

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {
		$cartId = $_POST['cartId'];
		$quantity = $_POST['quantity'];

		$updatequantity = $ct->updatequantity($cartId, $quantity);
		if ($quantity <= 0) {
			 $delcart = $ct->delcartbyid($cartId);
		}
}

// get Customer id from session

$cmrId = Session::get('cmrId');

// auto refresh page

if (!isset($_GET['CustomerId'])) {
	echo "<meta http-equiv='refresh' content='0;URL=?CustomerId=$cmrId'/>";
}

// get cart product

$getcartproduct = $ct->getcartproduct();
?>

<div class="main">
	<div class="content">
		<div class="cartoption">		
			<div class="cartpage">
				<h2>Your Cart</h2>
				<?php if(isset($updatequantity)) {
					 echo $updatequantity;
				} ?>
				<table class="tblone">
					<?php if (isset($delcart)) {
							echo $delcart;
					} ?>
					<tr>
						<th width="20%">Serial NO</th>
						<th width="20%">Product Name</th>
						<th width="10%">Image</th>
						<th width="15%">Price</th>
						<th width="25%">Quantity</th>
						<th width="20%">Total Price</th>
						<th width="10%">Action</th>
					</tr>

					<?php if ($getcartproduct) {
						$sum = 0;
						$quantity = 0;
						while ($getcart = $getcartproduct->fetch_assoc()) { ?>
							<tr>
								<td><?php echo $getcart['cartId']; ?></td>
								<td><?php echo $getcart['productName']; ?></td>
								<td><img src="admin/<?php echo $getcart['image']; ?>" alt=""/></td>
								<td>Tk. <?php echo $getcart['price']; ?></td>
								<td>
									<form action="" method="post">
										<input type="hidden" name="cartId" value="<?php echo $getcart['cartId']; ?>"/>
										<input type="number" name="quantity" value="<?php echo $getcart['quantity']; ?>"/>
										<input type="submit" name="submit" value="Update"/>
									</form>
								</td>
								<td>Tk. <?php $total = $getcart['price'] * $getcart['quantity']; echo $total;?> </td>
								<td><a onclick="return confirm('are you sure to delete!')" href="?delid=<?php echo $getcart['cartId']; ?>">X</a></td>
							</tr>
							<?php 
							$sum = $sum + $total; 
							$quantity = $quantity + $getcart['quantity'];
							Session::set('sum', $sum);
							Session::set('quantity', $quantity)
							?>
						<?php	}
					} ?>

				</table>
				<?php $getcart = $ct->checkcarttable();
				if ($getcart) { ?>
				<table style="float:right;text-align:left;" width="40%">
					<tr>
						<th>Sub Total : </th>
						<td>TK. <?php echo $sum = Session::get('sum');?></td>
					</tr>
					<tr>
						<th>VAT : </th>
						<td>TK. 5%</td>
					</tr>
					<tr>
						<th>Grand Total :</th>
						<td>TK. <?php 
						$vat = $sum * (5/100);
						$grandtotal = $sum + $vat;
						echo $grandtotal;

						 ?> </td>
					</tr>
				</table>
		    <?php	}else{
			echo "Cart empty!, Please shop now!";
		}?>
			</div>
			<div class="shopping">
				<div class="shopleft">
					<a href="index.php"> <img src="images/shop.png" alt="" /></a>
				</div>
				<div class="shopright">
					<a href="payment.php"> <img src="images/check.png" alt="" /></a>
				</div>
			</div>
		</div>  	
		<div class="clear"></div>
	</div>
</div>
</div>

<?php include "inc/footer.php"; ?>

