<?php 
include "inc/header.php";

// Delete Cart

if (isset($_GET['delid'])) {
	$cartId = $_GET['delid'];

	$delcart = $ct->delcartbyid($cartId);
}

// update Quantity 

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {
	$cartId = $_POST['cartId'];
	$quantity = $_POST['quantity'];

	$updatequantity = $ct->updatequantity($cartId, $quantity);
	if ($quantity <= 0) {
		$delcart = $ct->delcartbyid($cartId);
	}
}

// get Customer id from session

$cmrId = Session::get('cmrId');

// auto refresh page

if (!isset($_GET['CustomerId'])) {
	echo "<meta http-equiv='refresh' content='0;URL=?CustomerId=$cmrId'/>";
}

// get cart product

$getcartproduct = $ct->getcartproduct();



// get customer data

$getcustomer = $cmr->getCustomerData($cmrId);

// Order Now

if (isset($_GET['orderid']) && $_GET['orderid'] == 'order') {
	$cmrId = Session::get('cmrId');
	$getOrder = $ct->orderproduct($cmrId);
	$delordercart = $ct->delcartproduct();
}

?>
<style>
	.division { width: 47%; float: left; margin: 10px; border: 2px solid #ddd; border-radius: 3px; }
	.totaldiv { border-top: 3px dotted #ddd;}
	.totalpay{ float: right; text-align: left; width: 60%; margin-top: 14px;}
	.totalpay tr td{text-align: justify; padding: 7px 20px;}
	.totalpay tr th{text-align: justify; padding: 7px 20px;}
	.ordersection{ text-align: center; }
	.ordersection a {background: #eb3636;padding: 8px 18px;color: #fff;font-size: 18px;
}
</style>
<div class="main">
	<div class="content">
		<div class="section group">
			<div class="division">
				<table class="tblone">
					<tr>
						<th width="20%">Serial NO</th>
						<th width="20%">Product Name</th>
						<th width="10%">Image</th>
						<th width="15%">Price</th>
						<th width="25%">Quantity</th>
						<th width="20%">Total Price</th>
						<th width="10%">Action</th>
					</tr>

					<?php if ($getcartproduct) {
						$sum = 0;
						$quantity = 0;
						while ($getcart = $getcartproduct->fetch_assoc()) { ?>
							<tr>
								<td><?php echo $getcart['cartId']; ?></td>
								<td><?php echo $getcart['productName']; ?></td>
								<td><img src="admin/<?php echo $getcart['image']; ?>" alt=""/></td>
								<td>Tk. <?php echo $getcart['price']; ?></td>
								<td>
									<form action="" method="post">
										<input type="hidden" name="cartId" value="<?php echo $getcart['cartId']; ?>"/>
										<input type="number" name="quantity" value="<?php echo $getcart['quantity']; ?>"/>
										<input type="submit" name="submit" value="Update"/>
									</form>
								</td>
								<td>Tk. <?php $total = $getcart['price'] * $getcart['quantity']; echo $total;?> </td>
								<td><a onclick="return confirm('are you sure to delete!')" href="?delid=<?php echo $getcart['cartId']; ?>">X</a></td>
							</tr>
							<?php 
							$sum = $sum + $total; 
							$quantity = $quantity + $getcart['quantity'];
							Session::set('sum', $sum);
							Session::set('quantity', $quantity)
							?>
						<?php	}
					} ?>

				</table>
				<?php $getcart = $ct->checkcarttable();
				if ($getcart) { ?>
					<div class="totaldiv">
						<table class="totalpay">
							<tr>
								<th>Sub Total</th>
								<td> : </td>
								<td>TK. <?php echo $sum = Session::get('sum');?></td>
							</tr>
							<tr>
								<th>VAT</th>
								<td> : </td>
								<td>TK. 5%</td>
							</tr>
							<tr>
								<th>Grand Total</th>
								<td> : </td>
								<td>TK. <?php 
								$vat = $sum * (5/100);
								$grandtotal = $sum + $vat;
								echo $grandtotal;

								?> </td>
							</tr>
						</table>
					</div>
				<?php	}else{
					echo "Cart empty!, Please shop now!";
				}?>
			</div>
			<div class="division">
				<table class="tblone">
					<tr><td colspan="3"><h2>Profile Details</h2></td></tr>
					<?php if ($getcustomer) { 
						while($result = $getcustomer->fetch_assoc()){
							?>
							
							<tr>
								<td width="20%">Name</td>
								<td width="10%">:</td>
								<td width="30%"><?php echo $result['username'] ?></td>
							</tr>
							<tr>
								<td width="20%">Address</td>
								<td width="5%">:</td>
								<td width="30%"><?php echo $result['address'] ?></td>
							</tr>
							<tr>
								<td width="20%">City</td>
								<td width="5%">:</td>
								<td width="30%"><?php echo $result['city'] ?></td>
							</tr>
							<tr>
								<td width="20%">Country</td>
								<td width="5%">:</td>
								<td width="30%"><?php echo $result['country'] ?></td>
							</tr>
							<tr>
								<td width="20%">Zip</td>
								<td width="5%">:</td>
								<td width="30%"><?php echo $result['zip'] ?></td>
							</tr>
							<tr>
								<td width="20%">Phone</td>
								<td width="5%">:</td>
								<td width="30%"><?php echo $result['phone'] ?></td>
							</tr>
							<tr>
								<td width="20%">Phone</td>
								<td width="5%">:</td>
								<td width="30%"><?php echo $result['email'] ?></td>
							</tr>
							<tr>
								<td></td>
								<td width="50%"><a href="editprofile.php">Update Profile</a></td>
								<td></td>
							</tr>
						<?php }
					} ?>
				</table>
			</div>
		</div>
					<div class="ordersection">
				<a href="?orderid=order">Order Now</a>
			</div>
	</div>
</div>

<?php include "inc/footer.php"; ?>

