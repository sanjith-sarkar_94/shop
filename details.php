<?php include "inc/header.php"; 

// get product from index page

if (!isset($_GET['productId']) && $_GET['productId'] == NULL) {
	echo "<script>window.location = '404.php';</script>";
}else{
	$productId = $_GET['productId'];
}

//Requested method for Quantity update & add to cart

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['addtocart'])) {

	$quantity = $_POST['quantity'];

	$addcart = $ct->addtocart($quantity, $productId);
}



$getproduct = $pd->getsingleproduct($productId);

// Customer id from session 

$cmrId = Session::get('cmrId');


// Request Method for Wishlist

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['wlist'])) {
	
	$addwlist = $pd->addToWlist($productId, $cmrId);
}
?>

<style>
	.cartform{
		float: left;padding: 5px 5px 5px 0;
	}
</style>

<div class="main">
	<div class="content">
		<div class="section group">
			<?php 
			if ($getproduct) {
				while ($result = $getproduct->fetch_assoc()) { ?>
					<div class="cont-desc span_1_of_2">				
						<div class="grid images_3_of_2">
							<img src="admin/<?php echo $result['image']; ?>" alt="" />
						</div>
						<div class="desc span_3_of_2">
							
							<h2><?php echo $result['productName']; ?></h2>
							<p><?php echo $fm->textShorten($result['body']); ?></p>
							<div class="price">
								<p>Price: <span>$<?php echo $result['price']; ?></span></p>
								<p>Category: <span><?php echo $result['catName']; ?></span></p>
								<p>Brand:<span><?php echo $result['brandName']; ?></span></p>
							</div>
						<?php	}
					}
					?>				


					<div class="add-cart">
						<form action="" method="post">
							<input type="number" class="buyfield" name="quantity" value="1"/>
							<input type="submit" class="buysubmit" name="addtocart" value="Add to Cart"/>
						</form>				
					</div>
					<span style="color: red;">
						<?php 
						if (isset($addcart)) {
							echo $addcart;
						}
						?>
					</span>
					<div class="add-wlist">
						<form class="cartform" action="" method="post">						
							<input type="submit" class="buysubmit" name="wlist" value="Add to Wishlist"/>
						</form>
						<form class="cartform" action="payment.php" method="post">					
							<input type="submit" class="buysubmit" name="buynow" value="Buy Now"/>
						</form>
						<span style="color: red;">
							<?php 
							if (isset($addwlist)) {
								echo $addwlist;
							}
							?>
						</span>
					</div>
					
				</div>
				<div class="product-desc">
					<h2>Product Details</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
				</div>
				
			</div>
			<div class="rightsidebar span_3_of_1">
				<h2>CATEGORIES</h2>
				<ul>
					<li><a href="productbycat.html">Mobile Phones</a></li>
					<li><a href="productbycat.html">Desktop</a></li>
					<li><a href="productbycat.html">Laptop</a></li>
					<li><a href="productbycat.html">Accessories</a></li>
					<li><a href="productbycat.html#">Software</a></li>
					<li><a href="productbycat.html">Sports & Fitness</a></li>
					<li><a href="productbycat.html">Footwear</a></li>
					<li><a href="productbycat.html">Jewellery</a></li>
					<li><a href="productbycat.html">Clothing</a></li>
					<li><a href="productbycat.html">Home Decor & Kitchen</a></li>
					<li><a href="productbycat.html">Beauty & Healthcare</a></li>
					<li><a href="productbycat.html">Toys, Kids & Babies</a></li>
				</ul>
				
			</div>
		</div>
	</div>
</div>

<?php include "inc/footer.php"; ?>