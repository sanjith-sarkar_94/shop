<?php 
include "inc/header.php";
include "inc/slider.php";


$getfeaturedproduct = $pd->getfeaturedproduct();

$getnewproduct = $pd->getnewproduct();
 ?>


  <div class="main">
    <div class="content">
    	<div class="content_top">
    		<div class="heading">
    		<h3>Feature Products</h3>
    		</div>
    		<div class="clear"></div>
    	</div>
	      <div class="section group">
	      	<?php 
					if ($getfeaturedproduct) {
						while($result = $getfeaturedproduct->fetch_assoc()){ ?>
							<div class="grid_1_of_4 images_1_of_4">
					 <a href="details.php?productId=<?php echo $result['productId']; ?>"><img src="admin/<?php echo $result['image']; ?>" alt="image" /></a>
					 <h2><?php echo $result['productName']; ?></h2>
					 <p><?php echo $fm->textShorten($result['body'], 70); ?></p>
					 <p><span class="price">$<?php echo $result['price']; ?></span></p>
				     <div class="button"><span><a href="details.php?productId=<?php echo $result['productId']; ?>" class="details">Details</a></span></div>
				</div>
					<?php	}
					}
					 ?>
			</div>
			<div class="content_bottom">
    		<div class="heading">
    		<h3>New Products</h3>
    		</div>
    		<div class="clear"></div>
    	</div>
			<div class="section group">
				<?php 
				if ($getnewproduct) {
					while ($newproduct = $getnewproduct->fetch_assoc()) { ?>
						<div class="grid_1_of_4 images_1_of_4">
					 <a href="details.php?productId=<?php echo $newproduct['productId']; ?>"><img src="admin/<?php echo $newproduct['image']; ?>" alt="" /></a>
					 <h2><?php echo $newproduct['productName']; ?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
				     <div class="button"><span><a href="details.php?productId=<?php echo $newproduct['productId']; ?>" class="details">Details</a></span></div>
				</div>
				<?php }
				}
				 ?>
				
			</div>
    </div>
 </div>
</div>


<?php include "inc/footer.php";?>