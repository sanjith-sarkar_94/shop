<?php include "inc/header.php"; 

// Custom login

$login = Session::get('custlogin');
if ($login == false) {
	header("location: login.php");
}

// order list from cart for customer
$cmrId = Session::get('cmrId');
$getorder = $ct->getOrderFromCart($cmrId);
?>

<div class="main">
	<div class="content">
		<div class="section group">
			<table class="tblone">
				<?php if (isset($delcart)) {
					echo $delcart;
				} ?>
				<tr>
					<th width="5%">Serial</th>
					<th width="15%">Product Name</th>
					<th width="10%">Image</th>
					<th width="10%">Price</th>
					<th width="10%">Quantity</th>
					<th width="15%">Date</th>					
					<th width="20%">Total Price</th>
					<th width="15%">Status</th>
				</tr>

				<?php if ($getorder) {
					$sum = 0;
					$quantity = 0;
					while ($result = $getorder->fetch_assoc()) { ?>
						<tr>
							<td><?php echo $result['id']; ?></td>
							<td><?php echo $result['productName']; ?></td>
							<td><img src="admin/<?php echo $result['image']; ?>" width="70px" height="60px" alt=""/></td>
							<td>Tk. <?php echo $result['price']; ?></td>
							<td><?php echo $result['quantity']; ?></td>
							<td><?php echo $fm->dateFormat($result['date']); ?></td>
							<td>Tk. <?php $total = $result['price'] * $result['quantity']; echo $total;?> </td>
							<td>
								<?php if ($result['status'] == "0") {
									echo "Pending";
								}elseif($result['status'] == "1"){
									echo "Shiped";
								}else{
									echo "Delivered";
								} ?>
							</td>
						</tr>
						<?php 
						$sum = $sum + $total; 
						$quantity = $quantity + $result['quantity'];
						Session::set('sum', $sum);
						Session::set('quantity', $quantity)
						?>
					<?php	}
				} ?>

			</table>
		</div>
	</div>
</div>
<?php include "inc/footer.php"; ?>