<?php include "inc/header.php"; 

// get userid

$cmrId = Session::get('cmrId');

// get customer data

$getcustomer = $cmr->getCustomerData($cmrId);

// update customer profile

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['update'])) {
	
	$updateUser = $cmr->updateUserProfile($_POST, $cmrId);

}

?>
<style>
	.tblone{width: 550px; margin: 0 auto; border: 2px solid #ddd;}
	.tblone tr td{text-align: center;}
</style>
<div class="main">
	<div class="content">
		<div class="section group">			
			<form action="" method="post">
				<table class="tblone">
				<?php if ($getcustomer) { 
					while($result = $getcustomer->fetch_assoc()){
					?>
				<tr><td colspan="3"><h2>Profile Details</h2></td></tr>
				<tr>
					<td width="20%">Name</td>
					<td width="5%">:</td>
					<td width="30%"><input type="text" name="username" value="<?php echo $result['username'] ?>"></td>
				</tr>
				<tr>
					<td width="20%">Address</td>
					<td width="5%">:</td>
					<td width="30%"><input type="text" name="address" value="<?php echo $result['address'] ?>"></td>
				</tr>
				<tr>
					<td width="20%">City</td>
					<td width="5%">:</td>
					<td width="30%"><input type="text" name="city" value="<?php echo $result['city'] ?>"></td>
				</tr>
				<tr>
					<td width="20%">Country</td>
					<td width="5%">:</td>
					<td width="30%"><input type="text" name="country" value="<?php echo $result['country'] ?>"></td>
				</tr>
				<tr>
					<td width="20%">Zip</td>
					<td width="5%">:</td>
					<td width="30%"><input type="text" name="zip" value="<?php echo $result['zip'] ?>"></td>
				</tr>
				<tr>
					<td width="20%">Phone</td>
					<td width="5%">:</td>
					<td width="30%"><input type="text" name="phone" value="<?php echo $result['phone'] ?>"></td>
				</tr>
				<tr>
					<td width="20%">Phone</td>
					<td width="5%">:</td>
					<td width="30%"><input type="email" name="email" value="<?php echo $result['email'] ?>"></td>
				</tr>
				<tr>
					<td></td>
					
					<td width="50%"><input type="submit" name="update" value="Update"></td>
					<td></td>
				</tr>
					<?php }
				} ?>
			</table>
			</form>
		</div>
	</div>
</div>

<?php include "inc/footer.php"; ?>